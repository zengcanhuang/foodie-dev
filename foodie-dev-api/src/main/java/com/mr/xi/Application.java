package com.mr.xi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 1.@EnableScheduling 开启定时任务
 * 2.@MapperScan 扫描 mybatis 通用 mapper 所在的包
 * 3.@ComponentScan 扫描所有包以及相关组件包
 * @author Created By MrXi on 2019/12/18
 */
@SpringBootApplication
@MapperScan(basePackages = "com.mr.xi.mapper")
@ComponentScan(basePackages = {"com.mr.xi", "org.n3r.idworker"})
@EnableScheduling
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

}
