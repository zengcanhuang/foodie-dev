package com.mr.xi.controller;

import com.mr.xi.pojo.Orders;
import com.mr.xi.pojo.Users;
import com.mr.xi.pojo.vo.UsersVO;
import com.mr.xi.service.center.MyOrdersService;
import com.mr.xi.utils.RedisOperator;
import com.mr.xi.utils.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author Created By MrXi on 2020/5/7
 */
@Controller
public class BaseController {

    @Resource
    private RedisOperator redisOperator;

    public static final String FOODIE_SHOP_CART = "shop_cart";
    /**通用分页查询时,当pageSize为空时默认每页显示的条数*/
    public static final Integer COMMON_PAGE_SIZE = 10;
    /**通过关键字分页查询商品信息时,当pageSize为空时默认每页显示的条数*/
    public static final Integer PAGE_SIZE = 20;
    public static final String REDIS_USER_TOKEN = "redis_user_token";

    /**支付中心的调用地址*/
    String paymentUrl = "http://payment.t.mukewang.com/foodie-payment/payment/createMerchantOrder";

    /** 微信支付成功 -> 支付中心 -> 天天吃货平台
                          |-> 回调通知的url*/
    String payReturnUrl = "http://api.z.mukewang.com/foodie-dev-api/orders/notifyMerchantOrderPaid";

    // 用户上传头像的位置
//    public static final String IMAGE_USER_FACE_LOCATION = File.separator + "workspaces" +
//                                                            File.separator + "images" +
//                                                            File.separator + "foodie" +
//                                                            File.separator + "faces";
//    public static final String IMAGE_USER_FACE_LOCATION = "/workspaces/images/foodie/faces";


    @Resource
    public MyOrdersService myOrdersService;

    /**
     * 用于验证用户和订单是否有关联关系，避免非法用户调用
     * @return 返回验证结果
     */
    public Result checkUserOrder(String userId, String orderId) {
        Orders order = myOrdersService.queryMyOrder(userId, orderId);
        if (order == null) {
            return Result.errorMsg("订单不存在！");
        }
        return Result.ok(order);
    }

    public UsersVO conventUsersVO(Users user) {
        // 实现用户的redis会话
        String uniqueToken = UUID.randomUUID().toString().trim();
        redisOperator.set(REDIS_USER_TOKEN + ":" + user.getId(),
                uniqueToken);
        UsersVO usersVO = new UsersVO();
        BeanUtils.copyProperties(user, usersVO);
        usersVO.setUserUniqueToken(uniqueToken);
        return usersVO;
    }
}
