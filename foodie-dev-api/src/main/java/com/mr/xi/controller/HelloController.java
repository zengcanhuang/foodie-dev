package com.mr.xi.controller;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Created By MrXi on 2019/12/18
 */
@Api(value = "测试", tags = {"测试模块"})
@RestController
public class HelloController {

    private final static Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    @GetMapping("/hello")
    public String hello(){
        LOGGER.debug("debug: hello~");
        LOGGER.info("info: hello~");
        LOGGER.warn("warn: hello~");
        LOGGER.error("error: hello~");
        return "Hello World~";
    }


    @GetMapping("/setSession")
    public String setSession(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.setAttribute("userInfo","new user");
        session.setMaxInactiveInterval(3600);
        session.getAttribute("userInfo");
        session.removeAttribute("userInfo");
        return "OK";
    }
}
