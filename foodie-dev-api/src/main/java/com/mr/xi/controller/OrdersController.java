package com.mr.xi.controller;

import com.mr.xi.enums.OrderStatusEnum;
import com.mr.xi.enums.PayMethod;
import com.mr.xi.pojo.OrderStatus;
import com.mr.xi.pojo.bo.ShopCartBO;
import com.mr.xi.pojo.bo.SubmitOrderBO;
import com.mr.xi.pojo.vo.MerchantOrdersVO;
import com.mr.xi.pojo.vo.OrderVO;
import com.mr.xi.service.OrderService;
import com.mr.xi.utils.CookieUtils;
import com.mr.xi.utils.JsonUtils;
import com.mr.xi.utils.RedisOperator;
import com.mr.xi.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Created By MrXi on 2020/1/6
 */
@Api(value = "订单相关", tags = {"订单相关的api接口"})
@RequestMapping("orders")
@RestController
public class OrdersController extends BaseController {

    private final static Logger LOGGER = LoggerFactory.getLogger(OrdersController.class);

    @Resource
    private OrderService orderService;
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private RedisOperator redisOperator;

    @SuppressWarnings("all")
    @ApiOperation(value = "用户下单", notes = "用户下单", httpMethod = "POST")
    @PostMapping("/create")
    public Result create(
            @RequestBody SubmitOrderBO submitOrderBO,
            HttpServletRequest request,
            HttpServletResponse response) {

        if (submitOrderBO.getPayMethod().intValue() != PayMethod.WEIXIN.type
            && submitOrderBO.getPayMethod().intValue() != PayMethod.ALIPAY.type ) {
            return Result.errorMsg("支付方式不支持！");
        }
//        System.out.println(submitOrderBO.toString());
        LOGGER.info("submitOrderBO-->{}",submitOrderBO);
        String shopCartJson = redisOperator.get(FOODIE_SHOP_CART + ":" + submitOrderBO.getUserId());
        if(StringUtils.isBlank(shopCartJson)){
            return Result.errorMsg("购物数据不正确!");
        }
        List<ShopCartBO>  shopCartList = JsonUtils.jsonToList(shopCartJson, ShopCartBO.class);
        // 1. 创建订单
        OrderVO orderVO = orderService.createOrder(shopCartList,submitOrderBO);
        String orderId = orderVO.getOrderId();
        // 2. 创建订单以后，移除购物车中已结算（已提交）的商品
        /*
         * 1001
         * 2002 -> 用户购买
         * 3003 -> 用户购买
         * 4004
         */
        //清理覆盖现有的redis汇总的购物数据
        shopCartList.removeAll(orderVO.getToBeRemovedShopCatList());
        redisOperator.set(FOODIE_SHOP_CART+":"+submitOrderBO.getUserId(),JsonUtils.objectToJson(shopCartList));
        // 整合redis之后，完善购物车中的已结算商品清除，并且同步到前端的cookie
        CookieUtils.setCookie(request, response, FOODIE_SHOP_CART, JsonUtils.objectToJson(shopCartList), true);
        // 3. 向支付中心发送当前订单，用于保存支付中心的订单数据
        MerchantOrdersVO merchantOrdersVO = orderVO.getMerchantOrdersVO();
        merchantOrdersVO.setReturnUrl(payReturnUrl);
        // 为了方便测试购买，所以所有的支付金额都统一改为1分钱
        merchantOrdersVO.setAmount(1);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("xxxUserId","xxx");
        headers.add("password","xxx");
        HttpEntity<MerchantOrdersVO> entity =
                new HttpEntity<>(merchantOrdersVO, headers);
        ResponseEntity<Result> responseEntity =
                restTemplate.postForEntity(paymentUrl, entity, Result.class);
        Result paymentResult = responseEntity.getBody();
        if (paymentResult!=null && paymentResult.getStatus() != 200) {
            LOGGER.error("发送错误：{}", paymentResult.getMsg());
            return Result.errorMsg("支付中心订单创建失败，请联系管理员！");
        }
        return Result.ok(orderId);
    }

    @PostMapping("notifyMerchantOrderPaid")
    public Integer notifyMerchantOrderPaid(String merchantOrderId) {
        orderService.updateOrderStatus(merchantOrderId, OrderStatusEnum.WAIT_DELIVER.type);
        return HttpStatus.OK.value();
    }

    @PostMapping("getPaidOrderInfo")
    public Result getPaidOrderInfo(String orderId) {

        OrderStatus orderStatus = orderService.queryOrderStatusInfo(orderId);
        return Result.ok(orderStatus);
    }
}
