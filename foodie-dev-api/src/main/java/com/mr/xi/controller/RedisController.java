package com.mr.xi.controller;

import com.mr.xi.utils.RedisOperator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ApiIgnore
@RestController
@RequestMapping("redis")
public class RedisController {


    @Resource
    private RedisOperator redisOperator;

    @GetMapping("/set")
    public Object set(String key, String value) {
//        redisTemplate.opsForValue().set(key, value);
        redisOperator.set(key, value);
        return "OK";
    }

    @GetMapping("/get")
    public String get(String key) {
//        return (String)redisTemplate.opsForValue().get(key);
        return redisOperator.get(key);
    }

    @GetMapping("/delete")
    public Object delete(String key) {
//        redisTemplate.delete(key);
        redisOperator.del(key);
        return "OK";
    }

    /**
     * 大量key查询
     * @param keys 键值
     * @return 返回所有键对应的值
     */
    @GetMapping("/getALot")
    public Object getALot(String... keys) {
        List<String> result = new ArrayList<>();
        for (String k:keys) {
            result.add(redisOperator.get(k));
        }
        return result;
    }

    /**
     * 批量查询 mGet
     * @param keys 键值
     * @return 返回所有键对应的值
     */
    @GetMapping("/mGet")
    public Object mGet(String... keys) {
        List<String> keysList = Arrays.asList(keys);
        return redisOperator.mget(keysList);
    }

    /**
     * 批量查询 pipeline
     * @param keys 键值
     * @return 返回所有键对应的值
     */
    @GetMapping("/batchGet")
    public Object batchGet(String... keys) {
        List<String> keysList = Arrays.asList(keys);
        return redisOperator.batchGet(keysList);
    }

}