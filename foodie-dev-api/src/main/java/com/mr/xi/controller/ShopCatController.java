package com.mr.xi.controller;

import com.mr.xi.pojo.bo.ShopCartBO;
import com.mr.xi.utils.JsonUtils;
import com.mr.xi.utils.RedisOperator;
import com.mr.xi.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Api(value = "购物车接口controller", tags = {"购物车接口相关的api"})
@RequestMapping("shopCart")
@RestController
public class ShopCatController extends BaseController {

    @Resource
    private RedisOperator redisOperator;

    @ApiOperation(value = "添加商品到购物车", notes = "添加商品到购物车", httpMethod = "POST")
    @PostMapping("/add")
    public Result add(
            @RequestParam String userId,
            @RequestBody ShopCartBO shopcartBO) {

        if (StringUtils.isBlank(userId)) {
            return Result.errorMsg("");
        }

        System.out.println(shopcartBO);

        // 前端用户在登录的情况下，添加商品到购物车，会同时在后端同步购物车到redis缓存
        // 需要判断当前购物车中包含已经存在的商品，如果存在则累加购买数量
        String shopCartJson = redisOperator.get(FOODIE_SHOP_CART + ":" + userId);
        List<ShopCartBO> shopCartList;
        if (StringUtils.isNotBlank(shopCartJson)) {
            // redis中已经有购物车了
            shopCartList = JsonUtils.jsonToList(shopCartJson, ShopCartBO.class);
            // 判断购物车中是否存在已有商品，如果有的话counts累加
            boolean isHaving = false;
            if(shopCartList!=null){
                for (ShopCartBO sc: shopCartList) {
                    String tmpSpecId = sc.getSpecId();
                    if (tmpSpecId.equals(shopcartBO.getSpecId())) {
                        sc.setBuyCounts(sc.getBuyCounts() + shopcartBO.getBuyCounts());
                        isHaving = true;
                    }
                }
            }
            if (!isHaving&&shopCartList!=null) {
                shopCartList.add(shopcartBO);
            }
        } else {
            // redis中没有购物车
            shopCartList = new ArrayList<>();
            // 直接添加到购物车中
            shopCartList.add(shopcartBO);
        }

        // 覆盖现有redis中的购物车
        redisOperator.set(FOODIE_SHOP_CART + ":" + userId, JsonUtils.objectToJson(shopCartList));
        return Result.ok();
    }

    @ApiOperation(value = "从购物车中删除商品", notes = "从购物车中删除商品", httpMethod = "POST")
    @PostMapping("/del")
    public Result del(
            @RequestParam String userId,
            @RequestParam String itemSpecId) {

        if (StringUtils.isBlank(userId) || StringUtils.isBlank(itemSpecId)) {
            return Result.errorMsg("参数不能为空");
        }
        // 用户在页面删除购物车中的商品数据，如果此时用户已经登录，则需要同步删除后端购物车中的商品
        String shopCartJson = redisOperator.get(FOODIE_SHOP_CART + ":" + userId);
        if (StringUtils.isNotBlank(shopCartJson)) {
            // redis中已经有购物车了
            List<ShopCartBO> shopCartList = JsonUtils.jsonToList(shopCartJson, ShopCartBO.class);
            // 判断购物车中是否存在已有商品，如果有的话则删除
            if(shopCartList!=null){
                for (ShopCartBO sc: shopCartList) {
                    String tmpSpecId = sc.getSpecId();
                    if (tmpSpecId.equals(itemSpecId)) {
                        shopCartList.remove(sc);
                        break;
                    }
                }
            }
            // 覆盖现有redis中的购物车
            redisOperator.set(FOODIE_SHOP_CART + ":" + userId, JsonUtils.objectToJson(shopCartList));
        }
        return Result.ok();
    }

}
