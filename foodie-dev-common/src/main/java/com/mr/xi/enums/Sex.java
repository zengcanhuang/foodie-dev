package com.mr.xi.enums;

/**
 * <h1>性别枚举类</h1>
 * @author Created By MrXi on 2019/12/21
 */
public enum Sex {
    /**女*/
    WOMAN(0, "女"),
    /**男*/
    MAN(1, "男"),
    /**保密*/
    SECRET(2, "保密");

    public final Integer type;
    public final String value;

    Sex(Integer type, String value) {
        this.type = type;
        this.value = value;
    }
}
