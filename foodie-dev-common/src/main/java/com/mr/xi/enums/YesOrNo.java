package com.mr.xi.enums;

/**
 * 是否 枚举类
 * @author Created By MrXi on 2019/12/22
 */
public enum YesOrNo {
    /**否*/
    NO(0, "否"),
    /**是*/
    YES(1, "是");

    public final Integer type;
    public final String value;

    YesOrNo(Integer type, String value) {
        this.type = type;
        this.value = value;
    }
}
