package com.mr.xi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Created By MrXi on 2020/6/3
 */
@MapperScan(basePackages = "com.haijian.mapper")
@ComponentScan(basePackages = {"com.haijian", "org.n3r.idworker"})
@SpringBootApplication
public class FsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FsApplication.class,args);
    }

}
