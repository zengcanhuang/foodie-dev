package com.mr.xi.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Created By MrXi on 2020/6/3
 */
public interface FastDfsService {

    /**
     * 上传文件
     * @param file 文件
     * @param fileExtName 文件后缀名
     * @return 返回文件存储路径
     * @throws Exception 异常
     */
    String upload(MultipartFile file, String fileExtName) throws Exception;
}
