package com.mr.xi.service.impl;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.mr.xi.service.FastDfsService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;

/**
 * @author Created By MrXi on 2020/6/3
 */
@Service
public class FastServiceImpl implements FastDfsService {

    @Resource
    private FastFileStorageClient fastFileStorageClient;


    @Override
    public String upload(MultipartFile file,String fileExtName) throws Exception {
        StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(),
                file.getSize(), fileExtName, null);
        return storePath.getFullPath();
    }

}
