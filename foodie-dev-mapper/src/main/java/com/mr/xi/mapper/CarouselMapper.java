package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.Carousel;

public interface CarouselMapper extends MyMapper<Carousel> {
}