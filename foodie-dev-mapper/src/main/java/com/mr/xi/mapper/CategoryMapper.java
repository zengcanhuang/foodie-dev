package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.Category;

public interface CategoryMapper extends MyMapper<Category> {
}