package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.ItemsComments;

public interface ItemsCommentsMapper extends MyMapper<ItemsComments> {
}