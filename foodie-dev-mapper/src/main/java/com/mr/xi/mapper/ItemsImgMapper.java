package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.ItemsImg;

public interface ItemsImgMapper extends MyMapper<ItemsImg> {
}