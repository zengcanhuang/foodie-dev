package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.Items;

public interface ItemsMapper extends MyMapper<Items> {
}