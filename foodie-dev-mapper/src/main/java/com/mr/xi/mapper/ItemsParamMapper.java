package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.ItemsParam;

public interface ItemsParamMapper extends MyMapper<ItemsParam> {
}