package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.ItemsSpec;

public interface ItemsSpecMapper extends MyMapper<ItemsSpec> {
}