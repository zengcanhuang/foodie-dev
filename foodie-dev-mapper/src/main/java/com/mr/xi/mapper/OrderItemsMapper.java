package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.OrderItems;

public interface OrderItemsMapper extends MyMapper<OrderItems> {
}