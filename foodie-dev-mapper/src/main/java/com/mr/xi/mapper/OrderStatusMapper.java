package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.OrderStatus;

public interface OrderStatusMapper extends MyMapper<OrderStatus> {
}