package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.Orders;

public interface OrdersMapper extends MyMapper<Orders> {
}