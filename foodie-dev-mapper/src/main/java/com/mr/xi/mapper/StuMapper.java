package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.Stu;

public interface StuMapper extends MyMapper<Stu> {
}