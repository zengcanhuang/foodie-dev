package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.UserAddress;

public interface UserAddressMapper extends MyMapper<UserAddress> {
}