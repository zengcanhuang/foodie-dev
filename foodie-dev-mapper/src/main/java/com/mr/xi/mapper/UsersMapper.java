package com.mr.xi.mapper;

import com.mr.xi.my.mapper.MyMapper;
import com.mr.xi.pojo.Users;

public interface UsersMapper extends MyMapper<Users> {
}