package com.mr.xi.pojo.bo.center;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Created By MrXi on 2020/1/12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemsCommentBO {

    private String commentId;
    private String itemId;
    private String itemName;
    private String itemSpecId;
    private String itemSpecName;
    private Integer commentLevel;
    private String content;

}