package com.mr.xi.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * 最新商品VO
 * @author Created By MrXi on 2019/12/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewItemsVO {

    private Integer rootCatId;
    private String rootCatName;
    private String slogan;
    private String catImage;
    private String bgColor;

    private List<SimpleItemVO> simpleItemList;

}
