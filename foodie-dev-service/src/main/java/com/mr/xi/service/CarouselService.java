package com.mr.xi.service;

import com.mr.xi.pojo.Carousel;
import java.util.List;

public interface CarouselService {

    /**
     * 查询所有轮播图列表
     * @param isShow 是否显示
     * @return 返回所有的轮播图信息
     */
    List<Carousel> queryAll(Integer isShow);

}
