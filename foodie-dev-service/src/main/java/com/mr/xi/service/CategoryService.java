package com.mr.xi.service;

import com.mr.xi.pojo.Category;
import com.mr.xi.pojo.vo.CategoryVO;
import com.mr.xi.pojo.vo.NewItemsVO;
import java.util.List;

/**
 * @author Created By MrXi on 2019/12/22
 */
public interface CategoryService {

    /**
     * 查询所有一级分类
     * @return 返回所有一级分类
     */
    List<Category> queryAllRootLevelCat();

    /**
     * 根据一级分类id查询子分类信息
     * @param rootCatId 一级分类id
     * @return 返回一级分类的子分类集合
     */
    List<CategoryVO> getSubCatList(Integer rootCatId);

    /**
     * 查询首页每个一级分类下的6条最新商品数据
     * @param rootCatId 一级分类id
     * @return 返回首页每个一级分类下的6条最新商品数据
     */
    List<NewItemsVO> getSixNewItemsLazy(Integer rootCatId);

}
