package com.mr.xi.service;

import com.mr.xi.pojo.Items;
import com.mr.xi.pojo.ItemsImg;
import com.mr.xi.pojo.ItemsParam;
import com.mr.xi.pojo.ItemsSpec;
import com.mr.xi.pojo.vo.CommentLevelCountsVO;
import com.mr.xi.pojo.vo.ShopCartVO;
import com.mr.xi.utils.PagedGridResult;

import java.util.List;

/**
 * @author Created By MrXi on 2019/12/24
 */
public interface ItemService {

    /**
     * 根据商品ID查询详情
     * @param itemId 商品id
     * @return 返回商品详情信息
     */
    Items queryItemById(String itemId);

    /**
     * 根据商品id查询商品图片列表
     * @param itemId 商品id
     * @return 返回商品图片列表
     */
    List<ItemsImg> queryItemImgList(String itemId);

    /**
     * 根据商品id查询商品规格
     * @param itemId 商品id
     * @return 返回商品规格信息
     */
    List<ItemsSpec> queryItemSpecList(String itemId);

    /**
     * 根据商品id查询商品参数
     * @param itemId 商品id
     * @return 返回商品参数信息
     */
    ItemsParam queryItemParam(String itemId);

    /**
     * 根据商品id查询商品的评价等级数量
     * @param itemId 商品id
     * @return 返回商品的评价等级数量
     */
    CommentLevelCountsVO queryCommentCounts(String itemId);

    /**
     * 根据商品id查询商品的评价（分页）
     * @param itemId 商品id
     * @param level 评价等级
     * @param page 页码
     * @param pageSize 每页显示的条数
     * @return 返回商品的评价
     */
    PagedGridResult queryPagedComments(String itemId, Integer level,
                                       Integer page, Integer pageSize);

    /**
     * 搜索商品列表
     * @param keywords 搜索的关键字
     * @param sort 排序规则
     * @param page 页码
     * @param pageSize 每页显示的条数
     * @return 返回符合条件的商品信息
     */
    PagedGridResult searchItems(String keywords, String sort,
                                      Integer page, Integer pageSize);

    /**
     * 根据分类id搜索商品列表
     * @param catId 商品类别id
     * @param sort 排序规则
     * @param page 页码
     * @param pageSize 每页显示的条数
     * @return 返回符合条件的商品信息
     */
    PagedGridResult searchItems(Integer catId, String sort,
                                      Integer page, Integer pageSize);

    /**
     * 根据规格ids查询最新的购物车中商品数据（用于刷新渲染购物车中的商品数据）
     * @param specIds 规格id
     * @return 返回最新的商品数据信息
     */
    List<ShopCartVO> queryItemsBySpecIds(String specIds);

    /**
     * 根据商品规格id获取规格对象的具体信息
     * @param specId 商品规格id
     * @return 返回商品规格对象具体信息
     */
    ItemsSpec queryItemSpecById(String specId);

    /**
     * 根据商品id获得商品图片主图url
     * @param itemId 商品id
     * @return 返回商品图片主图url
     */
    String queryItemMainImgById(String itemId);

    /**
     * 减少库存
     * @param specId 商品规格id
     * @param buyCounts 购买数量
     */
    void decreaseItemSpecStock(String specId, int buyCounts);
}
