package com.mr.xi.service;

import com.mr.xi.pojo.OrderStatus;
import com.mr.xi.pojo.bo.ShopCartBO;
import com.mr.xi.pojo.bo.SubmitOrderBO;
import com.mr.xi.pojo.vo.OrderVO;

import java.util.List;

public interface OrderService {

    /**
     * 用于创建订单相关信息
     * @param shopCartList redis购物车数据
     * @param submitOrderBO 创建订单的BO对象参数
     * @return 返回创建订单的信息
     */
    OrderVO createOrder(List<ShopCartBO> shopCartList,SubmitOrderBO submitOrderBO);

    /**
     * 修改订单状态
     * @param orderId 订单id
     * @param orderStatus 订单状态
     */
    void updateOrderStatus(String orderId, Integer orderStatus);

    /**
     * 查询订单状态信息
     * @param orderId 订单id
     * @return 返回要查询的订单状态信息
     */
    OrderStatus queryOrderStatusInfo(String orderId);

    /**
     * 关闭超时未支付订单
     */
    void closeOrder();
}
