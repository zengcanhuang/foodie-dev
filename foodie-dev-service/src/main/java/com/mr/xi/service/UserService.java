package com.mr.xi.service;

import com.mr.xi.pojo.Users;
import com.mr.xi.pojo.bo.UserBO;

/**
 * @author Created By MrXi on 2019/12/21
 */
public interface UserService {

    /**
     * 判断用户名是否存在
     * @param username 用户名
     * @return true 用户名存在 false 用户名不存在
     */
    boolean queryUsernameIsExist(String username);

    /**
     * 创建用户(用户注册)
     * @param userBO 创建用户参数
     * @return users
     */
    Users createUser(UserBO userBO);

    /**
     * 检索用户名和密码是否匹配，用于登录
     * @param username 用户名
     * @param password 密码
     */
    Users queryUserForLogin(String username, String password);
}
