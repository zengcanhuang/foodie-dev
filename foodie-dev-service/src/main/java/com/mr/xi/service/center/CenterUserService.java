package com.mr.xi.service.center;

import com.mr.xi.pojo.Users;
import com.mr.xi.pojo.bo.center.CenterUserBO;

public interface CenterUserService {

    /**
     * 根据用户id查询用户信息
     * @param userId 用户id
     * @return 返回查询的用户信息
     */
    Users queryUserInfo(String userId);

    /**
     * 修改用户信息
     * @param userId 用户id
     * @param centerUserBO 前端传入的用户参数信息
     */
    Users updateUserInfo(String userId, CenterUserBO centerUserBO);

    /**
     * 用户头像更新
     * @param userId 用户id
     * @param faceUrl 用户头像地址
     * @return 返回用户信息
     */
    Users updateUserFace(String userId, String faceUrl);
}
