package com.mr.xi.service.center;

import com.mr.xi.pojo.OrderItems;
import com.mr.xi.pojo.bo.center.OrderItemsCommentBO;
import com.mr.xi.utils.PagedGridResult;

import java.util.List;

public interface MyCommentsService {

    /**
     * 根据订单id查询关联的商品
     * @param orderId 订单id
     * @return 返回和订单相关的商品
     */
    List<OrderItems> queryPendingComment(String orderId);

    /**
     * 保存用户的评论
     * @param orderId 订单id
     * @param userId 用户id
     * @param commentList 评论信息列表
     */
    void saveComments(String orderId, String userId, List<OrderItemsCommentBO> commentList);


    /**
     * 我的评价查询 分页
     * @param userId 用户id
     * @param page 页码
     * @param pageSize 每页显示的数量
     * @return 返回商品的评论列表
     */
    PagedGridResult queryMyComments(String userId, Integer page, Integer pageSize);
}
