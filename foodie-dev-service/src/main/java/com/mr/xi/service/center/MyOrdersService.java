package com.mr.xi.service.center;

import com.mr.xi.pojo.Orders;
import com.mr.xi.pojo.vo.OrderStatusCountsVO;
import com.mr.xi.utils.PagedGridResult;

public interface MyOrdersService {

    /**
     * 查询我的订单列表
     *
     * @param userId 用户id
     * @param orderStatus 订单状态
     * @param page 页码
     * @param pageSize 每页显示的数量
     * @return 返回相应状态的订单列表
     */
    PagedGridResult queryMyOrders(String userId,
                                  Integer orderStatus,
                                  Integer page,
                                  Integer pageSize);

    /**
     * 订单状态 --> 商家发货
     */
    void updateDeliverOrderStatus(String orderId);

    /**
     * 查询我的订单
     * @param userId 用户id
     * @param orderId 订单id
     * @return 返回订单信息
     */
    Orders queryMyOrder(String userId, String orderId);

    /**
     * 更新订单状态 —> 确认收货
     * @return 返回更新订单状态结果
     */
    boolean updateReceiveOrderStatus(String orderId);

    /**
     * 删除订单（逻辑删除）
     * @param userId 用户id
     * @param orderId 订单id
     * @return 返回删除结果
     */
    boolean deleteOrder(String userId, String orderId);

    /**
     * 查询用户订单状态数概况
     * @param userId 用户id
     */
    OrderStatusCountsVO getOrderStatusCounts(String userId);

    /**
     * 获得分页的订单动向
     * @param userId 用户id
     * @param page 页码
     * @param pageSize 每页显示的数量
     * @return 返回订单的动向数据
     */
    PagedGridResult getOrdersTrend(String userId,
                                          Integer page,
                                          Integer pageSize);
}