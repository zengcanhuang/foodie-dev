package com.mr.xi.service.impl.center;

import com.github.pagehelper.PageHelper;
import com.mr.xi.enums.YesOrNo;
import com.mr.xi.mapper.ItemsCommentsMapperCustom;
import com.mr.xi.mapper.OrderItemsMapper;
import com.mr.xi.mapper.OrderStatusMapper;
import com.mr.xi.mapper.OrdersMapper;
import com.mr.xi.pojo.OrderItems;
import com.mr.xi.pojo.OrderStatus;
import com.mr.xi.pojo.Orders;
import com.mr.xi.pojo.bo.center.OrderItemsCommentBO;
import com.mr.xi.pojo.vo.MyCommentVO;
import com.mr.xi.service.center.MyCommentsService;
import com.mr.xi.utils.PagedGridResult;
import org.n3r.idworker.Sid;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MyCommentsServiceImpl extends BaseService implements MyCommentsService {

    @Resource
    public OrderItemsMapper orderItemsMapper;

    @Resource
    public OrdersMapper ordersMapper;

    @Resource
    public OrderStatusMapper orderStatusMapper;

    @Resource
    public ItemsCommentsMapperCustom itemsCommentsMapperCustom;

    @Resource
    private Sid sid;

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public List<OrderItems> queryPendingComment(String orderId) {
        OrderItems query = new OrderItems();
        query.setOrderId(orderId);
        return orderItemsMapper.select(query);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void saveComments(String orderId, String userId,
                             List<OrderItemsCommentBO> commentList) {

        // 1. 保存评价 items_comments
        for (OrderItemsCommentBO oic : commentList) {
            oic.setCommentId(sid.nextShort());
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("commentList", commentList);
        itemsCommentsMapperCustom.saveComments(map);

        // 2. 修改订单表改已评价 orders
        Orders order = new Orders();
        order.setId(orderId);
        order.setIsComment(YesOrNo.YES.type);
        ordersMapper.updateByPrimaryKeySelective(order);

        // 3. 修改订单状态表的留言时间 order_status
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setOrderId(orderId);
        orderStatus.setCommentTime(new Date());
        orderStatusMapper.updateByPrimaryKeySelective(orderStatus);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedGridResult queryMyComments(String userId,
                                           Integer page,
                                           Integer pageSize) {

        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);

        PageHelper.startPage(page, pageSize);
        List<MyCommentVO> list = itemsCommentsMapperCustom.queryMyComments(map);

        return setterPagedGrid(list, page);
    }
}
