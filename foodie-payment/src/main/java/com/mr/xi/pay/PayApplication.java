package com.mr.xi.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 1.@EnableScheduling 开启定时任务
 * 2.@MapperScan 扫描 mybatis 通用 mapper 所在的包
 * 3.@ComponentScan 扫描 所有需要的包, 包含一些自用的工具类包 所在的路径
 * @author Created By MrXi on 2019/12/18
 */
@SpringBootApplication
@MapperScan(basePackages = "com.mr.xi.mapper")
@ComponentScan(basePackages= {"com.mr.xi", "org.n3r.idworker"})
public class PayApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(PayApplication.class, args);
	}
}
