package com.mr.xi.pay.mapper;

import com.mr.xi.pay.my.mapper.MyMapper;
import com.mr.xi.pay.pojo.Users;

public interface UsersMapper extends MyMapper<Users> {
}