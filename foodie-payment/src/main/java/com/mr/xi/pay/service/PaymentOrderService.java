package com.mr.xi.pay.service;

import com.mr.xi.pay.pojo.Orders;
import com.mr.xi.pay.pojo.bo.MerchantOrdersBO;

public interface PaymentOrderService {

	/**
	 * 创建支付中心的订单
	 */
	boolean createPaymentOrder(MerchantOrdersBO merchantOrdersBO);

	/**
	 * 查询未支付订单
	 */
	Orders queryOrderByStatus(String merchantUserId, String merchantOrderId, Integer orderStatus);

	/**
	 * 修改订单状态为已支付
	 */
	String updateOrderPaid(String merchantOrderId, Integer paidAmount);

	/**
	 * 查询订单信息
	 */
	Orders queryOrderInfo(String merchantUserId, String merchantOrderId);
}

