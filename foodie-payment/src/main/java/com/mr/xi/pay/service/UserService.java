package com.mr.xi.pay.service;

import com.mr.xi.pay.pojo.Users;

public interface UserService {

	/**
	 * @Description: 查询用户信息
	 */
	Users queryUserInfo(String userId, String pwd);

}

