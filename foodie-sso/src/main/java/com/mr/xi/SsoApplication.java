package com.mr.xi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Created By MrXi on 2020/5/7
 */
@SpringBootApplication
@MapperScan(basePackages = "com.haijian.mapper")
@ComponentScan(basePackages = {"com.haijian", "org.n3r.idworker"})
public class SsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoApplication.class,args);
    }

}
